# Cinnamoroll

This repo holds all the files to create certificate signing request using cert-manager and Venafi governance

# Moving parts
- [Some linux host](https://ubuntu.com/) 
- Kubernetes cluster ([k3s](https://k3s.io/) as example)
- [Cert-manager](/https://cert-manager.io/) installed by [helm](https://cert-manager.io/docs/installation/helm/)
- [Venafi](https://venafi.com) (pki governance - vaas for example)

# What is this? 

[![demo](https://asciinema.org/a//vlUW7IALeOM33fSzMbWkjwP0J.svg)](https://asciinema.org/a//vlUW7IALeOM33fSzMbWkjwP0J?autoplay=1)

If you'd like to try this yourself - read below:

# Setup some linux host 
Your laptop ;)

# Setup k3s (as example)
```
curl -sfL https://get.k3s.io | sh - 
```
Check for Ready node, takes ~30 seconds 
```
k3s kubectl get node
```

# Install cert-manager on k3s
```
export KUBECONFIG=[YOUR_KUBECONFIG_K3S]

helm repo add jetstack https://charts.jetstack.io; helm repo update
helm upgrade --install cert-manager jetstack/cert-manager --create-namespace --namespace cert-manager --version v1.10.1 --set installCRDs=true
```

# Venafi
- Create [VAAS](https://vaas.venafi.com/signup/) or local install of venafi
- Retrieve [api secret](https://docs.venafi.cloud/api/obtaining-api-key/) see 'vaas-secret-for-api.example' in repo
- Create [Appication](https://docs.venafi.cloud/vaas/application/creating-an-application/?h=create+applica)

# Demo apps
### Deploy two demo apps with certs
Note: create the vaas secret using 'see 'vaas-secret-for-api.example' in repo.

```
kubectl apply -f apple_certificate.yml
kubectl apply -f apple_demo.yml
kubectl apply -f banana_certificate.yml
kubectl apply -f banana_demo.yml
```

###  (In case you would like) delete two demo apps with certs
```
kubectl delete -f apple_certificate.yml
kubectl delete -f apple_demo.yml
kubectl delete secret/venafi-banana-djieno-default-com-tls


kubectl delete -f banana_certificate.yml
kubectl delete -f banana_demo.yml
kubectl delete secret/venafi-apple-djieno-default-com-tls
```

### Venafi Creating policies by vcert
Use 'mass' update or house keep policy [Venafi_vcert](https://github.com/Venafi/vcert/blob/master/README-POLICY-SPEC.md)
- See example in repo 'my_department_domains_to_policy.json'
[![demo](https://asciinema.org/a//YbE12ervB64oz3kgBfKtI4hDr.svg)](https://asciinema.org/a//YbE12ervB64oz3kgBfKtI4hDr?autoplay=1)


