$ kubectl create secret generic \
       vaas-secret \
       --namespace='NAMESPACE OF YOUR ISSUER RESOURCE' \
       --from-literal=apikey='YOUR_VAAS_API_KEY_HERE'
